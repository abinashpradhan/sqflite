import 'dart:convert';

import 'package:http/http.dart' as http;

import 'dashboardmodel.dart';

class DashBoardHelper {
  static late DashboardModel dashboardModel;
  static DashboardModel dashboardmodelGetter() {
    return dashboardModel;
  }

  static Future<DashboardModel> fetchDashBoard() async {
    print("successful0");
    final String apiUrl =
        "https://school.yoosys.in/yoosys_app/userDashboardDetails?androidrequest=true";
    final dashboardresponse = await http.post(Uri.parse(apiUrl),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          "userId": '15891',
        }));
    if (dashboardresponse.statusCode == 200) {
      final String dashboardresponseString = dashboardresponse.body;
      print("successful");
      dashboardModel = dashboardModelFromJson(dashboardresponseString);
      return dashboardModelFromJson(dashboardresponseString);
    } else {
      final String dashboardresponseString = dashboardresponse.body;
      return dashboardModelFromJson(dashboardresponseString);
    }
  }
}
