// To parse this JSON data, do
//
//     final dashboardModel = dashboardModelFromJson(jsonString);

import 'dart:convert';

DashboardModel dashboardModelFromJson(String str) =>
    DashboardModel.fromJson(json.decode(str));

String dashboardModelToJson(DashboardModel data) => json.encode(data.toJson());

class DashboardModel {
  DashboardModel({
    required this.dashboard,
  });

  List<Dashboard> dashboard;

  factory DashboardModel.fromJson(Map<dynamic, dynamic> json) => DashboardModel(
        dashboard: List<Dashboard>.from(
            json["dashboard"].map((x) => Dashboard.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "dashboard": List<dynamic>.from(dashboard.map((x) => x.toJson())),
      };
}

class Dashboard {
  Dashboard({
    this.roleId,
    this.roleName,
    this.userProfileDetails,
    this.studentDetailsList,
    this.teacherDetailsList,
    this.adminDetails,
  });

  int? roleId;
  String? roleName;
  UserProfileDetails? userProfileDetails;
  List<StudentDetailsList>? studentDetailsList;
  List<TeacherDetailsList>? teacherDetailsList;
  AdminDetails? adminDetails;

  factory Dashboard.fromJson(Map<String, dynamic> json) => Dashboard(
        roleId: json["roleId"],
        roleName: json["roleName"],
        userProfileDetails:
            UserProfileDetails.fromJson(json["userProfileDetails"]),
        studentDetailsList: json["studentDetailsList"] == null
            ? null
            : List<StudentDetailsList>.from(json["studentDetailsList"]
                .map((x) => StudentDetailsList.fromJson(x))),
        teacherDetailsList: json["teacherDetailsList"] == null
            ? null
            : List<TeacherDetailsList>.from(json["teacherDetailsList"]
                .map((x) => TeacherDetailsList.fromJson(x))),
        adminDetails: json["adminDetails"] == null
            ? null
            : AdminDetails.fromJson(json["adminDetails"]),
      );

  Map<String, dynamic> toJson() => {
        "roleId": roleId,
        "roleName": roleName,
        "userProfileDetails": userProfileDetails!.toJson(),
        "studentDetailsList": studentDetailsList == null
            ? null
            : List<dynamic>.from(studentDetailsList!.map((x) => x.toJson())),
        "teacherDetailsList": teacherDetailsList == null
            ? null
            : List<dynamic>.from(teacherDetailsList!.map((x) => x.toJson())),
        "adminDetails": adminDetails == null ? null : adminDetails!.toJson(),
      };
}

class AdminDetails {
  AdminDetails({
    this.totalStudents,
    this.totalMaleStudents,
    this.totalFemaleStudents,
    this.totalEmployees,
    this.totalTeachers,
    this.totalOfficeStaffs,
    this.totalOtherEmployees,
    this.todaysFeesCollection,
    this.classDetails,
    this.feesByType,
    this.schoolId,
    this.totalUsers,
  });

  int? totalStudents;
  int? totalMaleStudents;
  int? totalFemaleStudents;
  int? totalEmployees;
  int? totalTeachers;
  int? totalOfficeStaffs;
  int? totalOtherEmployees;
  double? todaysFeesCollection;
  List<ClassDetail>? classDetails;
  FeesByType? feesByType;
  int? schoolId;
  int? totalUsers;

  factory AdminDetails.fromJson(Map<String, dynamic> json) => AdminDetails(
        totalStudents: json["totalStudents"],
        totalMaleStudents: json["totalMaleStudents"],
        totalFemaleStudents: json["totalFemaleStudents"],
        totalEmployees: json["totalEmployees"],
        totalTeachers: json["totalTeachers"],
        totalOfficeStaffs: json["totalOfficeStaffs"],
        totalOtherEmployees: json["totalOtherEmployees"],
        todaysFeesCollection: json["todaysFeesCollection"],
        classDetails: List<ClassDetail>.from(
            json["classDetails"].map((x) => ClassDetail.fromJson(x))),
        feesByType: FeesByType.fromJson(json["feesByType"]),
        schoolId: json["schoolId"],
        totalUsers: json["totalUsers"],
      );

  Map<String, dynamic> toJson() => {
        "totalStudents": totalStudents,
        "totalMaleStudents": totalMaleStudents,
        "totalFemaleStudents": totalFemaleStudents,
        "totalEmployees": totalEmployees,
        "totalTeachers": totalTeachers,
        "totalOfficeStaffs": totalOfficeStaffs,
        "totalOtherEmployees": totalOtherEmployees,
        "todaysFeesCollection": todaysFeesCollection,
        "classDetails":
            List<dynamic>.from(classDetails!.map((x) => x.toJson())),
        "feesByType": feesByType!.toJson(),
        "schoolId": schoolId,
        "totalUsers": totalUsers,
      };
}

class ClassDetail {
  ClassDetail({
    this.className,
    this.strength,
    this.male,
    this.female,
  });

  String? className;
  int? strength;
  int? male;
  int? female;

  factory ClassDetail.fromJson(Map<String, dynamic> json) => ClassDetail(
        className: json["className"],
        strength: json["strength"],
        male: json["male"],
        female: json["female"],
      );

  Map<String, dynamic> toJson() => {
        "className": className,
        "strength": strength,
        "male": male,
        "female": female,
      };
}

class FeesByType {
  FeesByType({
    this.totalAmount,
    this.byCash,
    this.byCheque,
    this.byOnline,
    this.cashPercent,
    this.chequePercent,
    this.onlinePercent,
  });

  double? totalAmount;
  double? byCash;
  double? byCheque;
  double? byOnline;
  String? cashPercent;
  String? chequePercent;
  String? onlinePercent;

  factory FeesByType.fromJson(Map<String, dynamic> json) => FeesByType(
        totalAmount: json["totalAmount"],
        byCash: json["byCash"],
        byCheque: json["byCheque"],
        byOnline: json["byOnline"],
        cashPercent: json["cashPercent"],
        chequePercent: json["chequePercent"],
        onlinePercent: json["onlinePercent"],
      );

  Map<String, dynamic> toJson() => {
        "totalAmount": totalAmount,
        "byCash": byCash,
        "byCheque": byCheque,
        "byOnline": byOnline,
        "cashPercent": cashPercent,
        "chequePercent": chequePercent,
        "onlinePercent": onlinePercent,
      };
}

class StudentDetailsList {
  StudentDetailsList({
    this.studentId,
    this.userId,
    this.roleId,
    this.studentName,
    this.classId,
    this.className,
    this.sectionId,
    this.sectionName,
    this.schoolId,
    this.schoolName,
    this.schoolAddress,
    this.schoolLogo,
    this.rollNo,
    this.admissionNo,
    this.fatherName,
    this.motherName,
    this.address,
    this.bloodGroup,
    this.studentImage,
    this.contactNo,
    this.subjectList,
  });

  int? studentId;
  int? userId;
  int? roleId;
  String? studentName;
  int? classId;
  String? className;
  int? sectionId;
  String? sectionName;
  int? schoolId;
  String? schoolName;
  String? schoolAddress;
  dynamic? schoolLogo;
  int? rollNo;
  String? admissionNo;
  String? fatherName;
  String? motherName;
  String? address;
  String? bloodGroup;
  dynamic? studentImage;
  String? contactNo;
  List<SubjectList>? subjectList;

  factory StudentDetailsList.fromJson(Map<String, dynamic> json) =>
      StudentDetailsList(
        studentId: json["studentId"],
        userId: json["userId"],
        roleId: json["roleId"],
        studentName: json["studentName"],
        classId: json["classId"],
        className: json["className"],
        sectionId: json["sectionId"],
        sectionName: json["sectionName"],
        schoolId: json["schoolId"],
        schoolName: json["schoolName"],
        schoolAddress: json["schoolAddress"],
        schoolLogo: json["schoolLogo"],
        rollNo: json["rollNo"],
        admissionNo: json["admissionNo"],
        fatherName: json["fatherName"],
        motherName: json["motherName"],
        address: json["address"],
        bloodGroup: json["bloodGroup"],
        studentImage: json["studentImage"],
        contactNo: json["contactNo"],
        subjectList: List<SubjectList>.from(
            json["subjectList"].map((x) => SubjectList.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "studentId": studentId,
        "userId": userId,
        "roleId": roleId,
        "studentName": studentName,
        "classId": classId,
        "className": className,
        "sectionId": sectionId,
        "sectionName": sectionName,
        "schoolId": schoolId,
        "schoolName": schoolName,
        "schoolAddress": schoolAddress,
        "schoolLogo": schoolLogo,
        "rollNo": rollNo,
        "admissionNo": admissionNo,
        "fatherName": fatherName,
        "motherName": motherName,
        "address": address,
        "bloodGroup": bloodGroup,
        "studentImage": studentImage,
        "contactNo": contactNo,
        "subjectList": List<dynamic>.from(subjectList!.map((x) => x.toJson())),
      };
}

class SubjectList {
  SubjectList({
    this.notificationId,
    this.subjectId,
    this.subjectName,
  });

  int? notificationId;
  int? subjectId;
  String? subjectName;

  factory SubjectList.fromJson(Map<String, dynamic> json) => SubjectList(
        notificationId: json["notificationId"],
        subjectId: json["subjectId"],
        subjectName: json["subjectName"],
      );

  Map<String, dynamic> toJson() => {
        "notificationId": notificationId,
        "subjectId": subjectId,
        "subjectName": subjectName,
      };
}

class TeacherDetailsList {
  TeacherDetailsList({
    this.userId,
    this.employeeId,
    this.employeeName,
    this.classId,
    this.className,
    this.sectionId,
    this.sectionName,
    this.schoolId,
    this.schoolName,
    this.isClassTeacher,
    this.subjectList,
  });

  int? userId;
  int? employeeId;
  dynamic? employeeName;
  int? classId;
  String? className;
  int? sectionId;
  String? sectionName;
  int? schoolId;
  dynamic? schoolName;
  bool? isClassTeacher;
  List<SubjectList>? subjectList;

  factory TeacherDetailsList.fromJson(Map<String, dynamic> json) =>
      TeacherDetailsList(
        userId: json["userId"],
        employeeId: json["employeeId"],
        employeeName: json["employeeName"],
        classId: json["classId"],
        className: json["className"],
        sectionId: json["sectionId"],
        sectionName: json["sectionName"],
        schoolId: json["schoolId"],
        schoolName: json["schoolName"],
        isClassTeacher: json["isClassTeacher"],
        subjectList: List<SubjectList>.from(
            json["subjectList"].map((x) => SubjectList.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "userId": userId,
        "employeeId": employeeId,
        "employeeName": employeeName,
        "classId": classId,
        "className": className,
        "sectionId": sectionId,
        "sectionName": sectionName,
        "schoolId": schoolId,
        "schoolName": schoolName,
        "isClassTeacher": isClassTeacher,
        "subjectList": List<dynamic>.from(subjectList!.map((x) => x.toJson())),
      };
}

class UserProfileDetails {
  UserProfileDetails({
    this.userId,
    this.roleId,
    this.userName,
    this.password,
    this.otp,
    this.newPassword,
    this.emailId,
    this.mobileNo,
    this.userFullName,
    this.firebaseToken,
    this.deviceId,
    this.osType,
    this.logoutStatus,
    this.schoolId,
    this.schoolName,
    this.gdriveUrl,
    this.paymentUrl,
  });

  int? userId;
  int? roleId;
  String? userName;
  dynamic? password;
  int? otp;
  dynamic? newPassword;
  String? emailId;
  String? mobileNo;
  String? userFullName;
  dynamic? firebaseToken;
  dynamic? deviceId;
  dynamic? osType;
  int? logoutStatus;
  int? schoolId;
  String? schoolName;
  String? gdriveUrl;
  String? paymentUrl;

  factory UserProfileDetails.fromJson(Map<String, dynamic> json) =>
      UserProfileDetails(
        userId: json["userId"],
        roleId: json["roleId"],
        userName: json["userName"],
        password: json["password"],
        otp: json["otp"],
        newPassword: json["newPassword"],
        emailId: json["emailId"],
        mobileNo: json["mobileNo"],
        userFullName: json["userFullName"],
        firebaseToken: json["firebaseToken"],
        deviceId: json["deviceId"],
        osType: json["osType"],
        logoutStatus: json["logoutStatus"],
        schoolId: json["schoolId"],
        schoolName: json["schoolName"],
        gdriveUrl: json["gdriveUrl"] == null ? null : json["gdriveUrl"],
        paymentUrl: json["paymentUrl"] == null ? null : json["paymentUrl"],
      );

  Map<String, dynamic> toJson() => {
        "userId": userId,
        "roleId": roleId,
        "userName": userName,
        "password": password,
        "otp": otp,
        "newPassword": newPassword,
        "emailId": emailId,
        "mobileNo": mobileNo,
        "userFullName": userFullName,
        "firebaseToken": firebaseToken,
        "deviceId": deviceId,
        "osType": osType,
        "logoutStatus": logoutStatus,
        "schoolId": schoolId,
        "schoolName": schoolName,
        "gdriveUrl": gdriveUrl == null ? null : gdriveUrl,
        "paymentUrl": paymentUrl == null ? null : paymentUrl,
      };
}
