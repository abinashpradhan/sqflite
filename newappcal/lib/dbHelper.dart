import 'dart:async';
import 'dart:io' as io;
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'dashboardmodel.dart';

class DBHelper {
  static late Database _db;
  // Create the Table colums
  static const String TABLE = 'dashboard';
  static const String ROLE_ID = 'roleId';
  static const String ID = 'id';
  static const String ROLE_NAME = 'roleName';

  //DASHBOARD
  static const String DashboardiD = "id";
  static const String DashboardroleId = "roleId";
  static const String DashboardroleName = "roleName";
  static const String DashboarduserProfileDetails = "userProfileDetails";
  static const String DashboardstudentDetailsList = "studentDetailsList";
  static const String DashboardteacherDetailsList = "teacherDetailsList";
  static const String DashboardadminDetails = "adminDetails";
  //UserProfileDetails DATA
  static const String UserProfileDetailsiD = "id";
  static const String UserProfileDetailstABLEnAME = 'UserProfileDetails';
  static const String UserProfileDetailsuserId = "userId";
  static const String UserProfileDetailsroleId = "roleId";
  static const String UserProfileDetailsuserName = "userName";
  static const String UserProfileDetailspassword = "password";
  static const String UserProfileDetailsotp = "otp";
  static const String UserProfileDetailsnewPassword = "newPassword";
  static const String UserProfileDetailsemailId = "emailId";
  static const String UserProfileDetailsmobileNo = "mobileNo";
  static const String UserProfileDetailsuserFullName = "userFullName";
  static const String UserProfileDetailsfirebaseToken = "firebaseToken";
  static const String UserProfileDetailsdeviceId = "deviceId";
  static const String UserProfileDetailsosType = "osType";
  static const String UserProfileDetailslogoutStatus = "logoutStatus";
  static const String UserProfileDetailsschoolId = "schoolId";
  static const String UserProfileDetailsschoolName = "schoolName";
  static const String UserProfileDetailsgdriveUrl = "gdriveUrl";
  static const String UserProfileDetailspaymentUrl = "paymentUrl";

  //SUBJECTLIST
  static const String SubjectListiD = "ID";
  static const String SubjectListnotificationId = "notificationId";
  static const String SubjectListsubjectId = "subjectId";
  static const String SubjectListsubjectName = "subjectName";

  //TeacherDetailsList
  static const String TeacherDetailsiD = "ID";
  static const String TeacherDetailsListuserId = "userId";
  static const String TeacherDetailsListemployeeId = "employeeId";
  static const String TeacherDetailsListemployeeName = "employeeName";
  static const String TeacherDetailsListclassId = "classId";
  static const String TeacherDetailsListclassName = "className";
  static const String TeacherDetailsListsectionId = "sectionId";
  static const String TeacherDetailsListsectionName = "sectionName";
  static const String TeacherDetailsListschoolId = "schoolId";
  static const String TeacherDetailsListschoolName = "schoolName";
  static const String TeacherDetailsListisClassTeacher = "isClassTeacher";
  static const String TeacherDetailsListsubjectList =
      "subjectList"; //IT IS A FORGAIN KEY

  //StudentDetailsList
  static const String StudentDetailsListiD = "ID";
  static const String StudentDetailsListstudentId = "studentId";
  static const String StudentDetailsListuserId = "userId";
  static const String StudentDetailsListroleId = "roleId";
  static const String StudentDetailsListstudentName = "studentName";
  static const String StudentDetailsListclassId = "classId";
  static const String StudentDetailsListclassName = "className";
  static const String StudentDetailsListsectionId = "sectionId";
  static const String StudentDetailsListsectionName = "sectionName";
  static const String StudentDetailsListschoolId = "schoolId";
  static const String StudentDetailsListschoolName = "schoolName";
  static const String StudentDetailsListschoolAddress = "schoolAddress";
  static const String StudentDetailsListschoolLogo = "schoolLogo";
  static const String StudentDetailsListrollNo = "rollNo";
  static const String StudentDetailsListadmissionNo = "admissionNo";
  static const String StudentDetailsListfatherName = "fatherName";
  static const String StudentDetailsListmotherName = "motherName";
  static const String StudentDetailsListaddress = "address";
  static const String StudentDetailsListbloodGroup = "bloodGroup";
  static const String StudentDetailsListstudentImage = "studentImage";
  static const String StudentDetailsListcontactNo = "contactNo";
  static const String StudentDetailsListsubjectList =
      "subjectList"; //FORGAIN KEY

  //FeesByType
  static const String FeesByTypeiD = "ID";
  static const String FeesByTypetotalAmount = "totalAmount";
  static const String FeesByTypebyCash = "byCash";
  static const String FeesByTypebyCheque = "byCheque";
  static const String FeesByTypebyOnline = "byOnline";
  static const String FeesByTypecashPercent = "cashPercent";
  static const String FeesByTypechequePercent = "chequePercent";
  static const String FeesByTypeonlinePercent = "onlinePercent";

  //ClassDetail
  static const String ClassDetailiD = "ID";
  static const String ClassDetailclassName = "className";
  static const String ClassDetailstrength = "strength";
  static const String ClassDetailmale = "male";
  static const String ClassDetailfemale = "female";

  //AdminDetails
  static const String AdminDetailsiD = "ID";
  static const String AdminDetailstotalStudents = "totalStudents";
  static const String AdminDetailstotalMaleStudents = "totalMaleStudents";
  static const String AdminDetailstotalFemaleStudents = "totalFemaleStudents";
  static const String AdminDetailstotalEmployees = "totalEmployees";
  static const String AdminDetailstotalTeachers = "totalTeachers";
  static const String AdminDetailstotalOfficeStaffs = "totalOfficeStaffs";
  static const String AdminDetailstotalOtherEmployees = "totalOtherEmployees";
  static const String AdminDetailstodaysFeesCollection = "todaysFeesCollection";
  static const String AdminDetailsclassDetails = "classDetails";
  static const String AdminDetailsfeesByType = "feesByType";
  static const String AdminDetailsschoolId = "schoolId";
  static const String AdminDetailstotalUsers = "totalUsers";

  static const String DB_NAME = 'dashboard.db';

  // Initialize the Database
  Future<Database> get db async {
    if (null != _db) {
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  initDb() async {
    // Get the Device's Documents directory to store the Offline Database...
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, DB_NAME);
    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  _onCreate(Database db, int version) async {
    // Create the DB Table
    await db.execute(
        "CREATE TABLE $TABLE ($ID INTEGER PRIMARY KEY, $ROLE_ID TEXT, $ROLE_NAME TEXT)");
    await db.execute(
      "CREATE TABLE $UserProfileDetailstABLEnAME($UserProfileDetailsiD INTEGER PRIMARY KEY ,$UserProfileDetailsuserId INT, )",
    );
  }

  // Method to insert the Album record to the Database
  Future<DashboardModel> save(DashboardModel dashboardModel) async {
    var dbClient = await db;
    // this will insert the Album object to the DB after converting it to a json
    //dashboardModel.roleId = await dbClient.insert(TABLE, dashboardModel.toJson());
    dashboardModel.dashboard[0].roleId =
        await dbClient.insert(TABLE, dashboardModel.toJson());
    return dashboardModel;
  }

  // Method to return all Albums from the DB
  Future<DashboardModel> getAlbums() async {
    var dbClient = await db;
    // specify the column names you want in the result set
    List<Map> maps = await dbClient.query(TABLE, columns: [ID, ROLE_NAME]);
    DashboardModel allAlbums = DashboardModel(dashboard: []);
    List<DashboardModel> albums = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        albums.add(DashboardModel.fromJson(maps[i]));
      }
    }
    allAlbums = albums as DashboardModel;
    return allAlbums;
  }

  // Method to delete an Album from the Database
  Future<int> delete(int id) async {
    var dbClient = await db;
    return await dbClient.delete(TABLE, where: '$ID = ?', whereArgs: [id]);
  }

  // Method to Update an Album in the Database
  Future<int> update(DashboardModel album) async {
    var dbClient = await db;
    return await dbClient.update(TABLE, album.toJson(),
        where: '$ID = ?', whereArgs: [album.dashboard[0].roleId]);
  }

  // Method to Truncate the Table
  Future<int> truncateTable() async {
    var dbClient = await db;
    return await dbClient.delete(TABLE);
  }

  // Method to Close the Database
  Future close() async {
    var dbClient = await db;
    dbClient.close();
  }
}
